const config = {
    // Backend config
    s3: {
    REGION: 'us-east-1',
    BUCKET: 'peggys-lunar-tour-api-de-serverlessdeploymentbuck-ikikpeb2an0l',
    },
    apiGateway: {
    REGION: 'us-east-1',
    URL: 'https://36eh346i8c.execute-api.us-east-1.amazonaws.com/graphql',
    },
    cognito: {
    REGION:'us-east-1',
    USER_POOL_ID: 'us-east-1_8UadveXt4',
    APP_CLIENT_ID: 'qi0u8ofq8l9pm4n75gvijvvll',
    IDENTITY_POOL_ID: 'us-east-1:035afec5-95ee-4dbb-8821-34f1fb2e650e ',
    },
};

export default config;
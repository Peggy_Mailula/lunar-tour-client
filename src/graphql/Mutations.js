import gql from "graphql-tag";

export const UPDATE_FORM_DATA = gql`
mutation UPDATE_FORM_DATA(
    $date: String
    $email: String
    $customers: InputCustomer
    
    
) {
    updateFormData(date: $date, email: $email, customers: $customer) @client
}
`;

export const MAKE_A_BOOKING = gql`
mutation MAKE_A_BOOKING(
    $customerEmail: String
    $bookingDate: String
    $listingId: String
    $customers: [InputCustomer]

) {
    makeABooking(
    customerEmail: $customerEmail
    bookingDate: $bookingDate
    listingId: $listingId
    customers: $customers
    ) {
    bookingId
    listingID
    bookingDate
    bookingTotal
    customerEmail
    customers {
        name
        Surname
        country
        passportNumber
        physioScore
    }
    chargeReciept
    }
}
`;
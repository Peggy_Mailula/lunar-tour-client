import React from "react";
import { Router } from "@reach/router";
import Home from "./pages/index";
import ViewListing from "./pages/ViewListing"
import BookingIndex from "./pages/booking"
import Login from "./pages/auth/Login";
import Signup from "./pages/auth/Signup";



const Routes = ({ props }) => {
  return (
    <Router>
      <Login exact path = "/login" />
      <Home path="/" props={props} />
        <Signup exact path="/signup"/>
      <ViewListing path="/listing/:id" props={props} />
      <BookingIndex path="/booking/:id" props={props} />
    </Router>
    
 
 
  );
};

export default Routes;


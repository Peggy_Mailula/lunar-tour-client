import { AppContext } from "./lib/contextLib";
import React , { useState, useEffect }from "react";
import { Auth } from "aws-amplify";
import Routes from "./routes";
import { navigate } from "@reach/router"
//import Nav from "./components/navs /Nav";

import { LinkContainer } from "react-router-bootstrap";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";




const App = () => {
    
    const [isAuthenticating, setIsAuthenticating] = useState(true);
    const [isAuthenticated, userHasAuthenticated] = useState(false); 


    async function handleLogout() {
        await Auth.signOut();
        userHasAuthenticated(false);
        navigate(`/login`)
        }
    
    useEffect(() => {
        onLoad();
    }, []);
    
    async function onLoad() {
        try {
            await Auth.currentSession();
            userHasAuthenticated(true);
        }
        catch(e) {
            if (e !== 'No current user') {
            alert(e);
            }
        }

        setIsAuthenticating(false);
        }

        const signup = ()=>{
            userHasAuthenticated(false);
            navigate(`/signup`)
        }

        
        return (
            !isAuthenticating && (
                <div >
                <Navbar>
                    <Navbar.Toggle />
            
                    <Nav activeKey={window.location.pathname}>
                        {isAuthenticated ? (
                        <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
                        ) : (
                        <>
                            <LinkContainer to="/signup">
                            <Nav.Link onClick={signup}>Signup </Nav.Link>
                            </LinkContainer>
                            <LinkContainer to="/login">
                            <Nav.Link onClick = {handleLogout}> Login</Nav.Link>
                            </LinkContainer>
                        </>
                        )}
                    </Nav>
                </Navbar>
                
                <AppContext.Provider value={{ isAuthenticated, userHasAuthenticated }}>
                    <Routes />
    
                </AppContext.Provider>
                </div>
            )
            );
};
export default App;
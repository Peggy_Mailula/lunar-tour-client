import React from "react"
import { useMutation } from "@apollo/react-hooks"
import { UPDATE_FORM_DATA } from "../../graphql/Mutations"
import HeadingOne from "../../components/typography/HeadingOne"
import BodyOne from "../../components/typography/BodyOne"
import { useState } from 'react'
import RedBlockButton from "../../components/buttons/RedBlockButton"
import RedOutlineButton from "../../components/buttons/RedOutlineButton"
import Input from "../../components/inputs/input"
import { Form } from "antd"
import "antd/dist/antd.css";
import validator from 'validator'



const CustomerDetails = props => {
console.log(props)

const [mutate] = useMutation(UPDATE_FORM_DATA)
const [email, setEmail] = useState('');



const validateEmail = (e) =>{
    var emails = e.target.value

    if(validator.isEmail(emails)){
        setEmail(emails)

    }else{
        setEmail('')
    }

}
const onFinish = () => {
    if(email ===''){
    alert('Please enter your email')
    }
    else{
        props.setActiveTab("2")
    }

};


return (
    <>
    <Form
        onValuesChange={(allValues,) => {
        mutate({
            variables: {
            email: allValues.email,
            date: allValues.date,
            },
        })
        
        }}
    
    >
        <div className="flex flex-col p-20 ">
        <HeadingOne>Booking for Listing Name</HeadingOne>
        <div className="mt-5">
            <BodyOne>Booking date</BodyOne>
            <Form.Item name="date">
            <Input placeholder="date" type="date"  />
                </Form.Item>
            
        </div>

        <div className="mt-5">
            <BodyOne> Email address </BodyOne>
            <Form.Item name="email"  rules={[
            {
            type: "email",
            required: true,
            message: "Please enter a valid email format",
            }
        ]}
        >
            <Input placeholder="doku@corrisant.io" type="email"  onChange= {(e)=>validateEmail(e)} 
            /> 
        
    
            </Form.Item>
        </div>
        
        <div className="flex lg:flex-row mt-5 s:flex-col">
    
<RedBlockButton className="mr-5 s:mb-5 lg:mb-0" onClick={onFinish}>
            Proceed
            </RedBlockButton>

<RedOutlineButton >Cancel</RedOutlineButton>
        </div>
        </div>
    </Form>
    </>
)
}

export default CustomerDetails
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import { Auth } from "aws-amplify";
import { navigate } from "@reach/router"
import LoaderButton from "../../components/buttons/LoaderButton";
import { useAppContext } from "../../lib/contextLib";
import { useFormFields } from "../../lib/hooksLib";
import { onError } from "../../lib/errorLib";
import "./Signup.css";
import { BodyOne } from "../../components/typography";
import Nav from '../../components/navs /Nav'

export default function Signup() {
    const [fields, handleFieldChange] = useFormFields({
    email: "",
    password: "",
    confirmPassword: "",
    confirmationCode: "",
    });
    
    const [newUser, setNewUser] = useState(null);
    const { userHasAuthenticated } = useAppContext();
    const [isLoading, setIsLoading] = useState(false);

function validateForm() {
    return (
        fields.email.length > 0 &&
        fields.password.length > 0 &&
        fields.password === fields.confirmPassword
    );
}

async function handleSubmit(event) {
    event.preventDefault();

    setIsLoading(true);

    try {
        const newUser = await Auth.signUp({
        username: fields.email,
        password: fields.password,
        });
        setIsLoading(false);
        setNewUser(newUser);
    } catch (e) {
        onError(e);
        setIsLoading(false);
    }
    }
    async function handleConfirmationSubmit(event) {
        event.preventDefault();
        setIsLoading(true);

        try {
            await Auth.confirmSignUp(fields.email, fields.confirmationCode);
            await Auth.signIn(fields.email, fields.password);
            userHasAuthenticated(true);
            navigate(`/`)
        } catch (e) {
            onError(e);
            setIsLoading(false);
        }
        }

function renderConfirmationForm() {
    return (
        <Form onSubmit={handleConfirmationSubmit}>
            <Nav/>
        <Form.Group controlId="confirmationCode" size="lg">
            <BodyOne>Confirmation Code</BodyOne>
            <Form.Control
            autoFocus
            type="tel"
            onChange={handleFieldChange}
            value={fields.confirmationCode}
            />
            <Form.Text muted>Please check your email for the code.</Form.Text>
        </Form.Group>
        <LoaderButton
            block
            size="lg"
            type="submit"
            variant="success"
            isLoading={isLoading}
            disabled={!validateForm()}
        >
            Verify
        </LoaderButton>
        </Form>
    );
    }

    function renderForm() {
    return (
        
        <Form onSubmit={handleSubmit}>
        <Nav/>
        
        <Form.Group controlId="email" size="lg">
            <BodyOne>Email</BodyOne>
            <Form.Control className="field"
            autoFocus
            type="email"
            value={fields.email}
            onChange={handleFieldChange}
            />
        </Form.Group>
        <Form.Group controlId="password" size="lg">
            <BodyOne>Password</BodyOne>
            <Form.Control className="field"
            type="password"
            value={fields.password}
            onChange={handleFieldChange}
            />
        </Form.Group>
        <Form.Group controlId="confirmPassword" size="lg">
            <BodyOne>Confirm Password</BodyOne>
            <Form.Control className="field"
            type="password"
            onChange={handleFieldChange}
            value={fields.confirmPassword}
            />
        </Form.Group>
        <br/>
        <LoaderButton
            block
            size="lg"
            type="submit"
            variant="success"
            isLoading={isLoading}
            disabled={!validateForm()}
        >
            Signup
        </LoaderButton>
        </Form>
    );
    }

    return (
    <div className="Signup">
        {newUser === null ? renderForm() : renderConfirmationForm()}
    </div>
    );
}
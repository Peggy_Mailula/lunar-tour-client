import React, { useState } from "react";
import { Auth } from "aws-amplify";
import Form from "react-bootstrap/Form";
import { navigate } from "@reach/router"
import LoaderButton from "../../components/buttons/LoaderButton";
import { useAppContext } from "../../lib/contextLib";
import { useFormFields } from "../../lib/hooksLib";
import { onError } from "../../lib/errorLib";
import "./Login.css";
import BodyOne from '../../components/typography/BodyOne'
import Nav from '../../components/navs /Nav'


export default function Login() {
  
  const { userHasAuthenticated } = useAppContext();
  const [isLoading, setIsLoading] = useState(false);
  const [fields, handleFieldChange] = useFormFields({
    email: "",
    password: ""
  });

  function validateForm() {
    return fields.email.length > 0 && fields.password.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();

    setIsLoading(true);

    try {
      await Auth.signIn(fields.email, fields.password);
      userHasAuthenticated(true);
      navigate(`/`)
      
    } catch (e) {
      onError(e);
      setIsLoading(false);
    }
  }

  return (
    <div className="Login">
      <Nav/>
      <Form onSubmit={handleSubmit}>
        <Form.Group size="lg" controlId="email">
          <BodyOne>Email</BodyOne>
          <Form.Control className="field"
            autoFocus
            type="email"
            value={fields.email}
            onChange={handleFieldChange}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <BodyOne>Password</BodyOne>
          <Form.Control class="form-control" className="field"
            type="password"

            onChange={handleFieldChange}
          />
        </Form.Group>
        <br/>
        <LoaderButton
          block
          size="lg"
          type="submit"
          isLoading={isLoading}
          disabled={!validateForm()}
        
        >
          Login
        </LoaderButton>
      </Form>
    </div>
  );
}
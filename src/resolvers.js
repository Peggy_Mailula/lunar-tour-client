import { GET_FORM_DATA } from "./graphql/Queries"

export const resolvers = {
Mutation: {
    updateFormData: (parent, args, context, info) => {
    const queryResult = context.cache.readQuery({ query: GET_FORM_DATA })
    const { formData } = queryResult
    console.log('Arguments errors',args)
    if (queryResult) {
        const data = {
        formData: {
            date: args.date,
            email: args.email,
            customer: args.customers,
            __typename: formData["__typename"],
        },
        }

        context.cache.writeQuery({ query: GET_FORM_DATA, data })
        return data.formData
    }
    return []
    },
},

}
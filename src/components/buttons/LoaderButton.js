import React from "react";
import RedBlockButton from "./RedBlockButton";
import { BsArrowRepeat } from "react-icons/bs";
import "./LoaderButton.css";

const LoaderButton = ({
  isLoading,
  className = "",
  disabled = false,
  ...props
})=> 
{
  return (
    <RedBlockButton
      disabled={disabled || isLoading}
      className={`LoaderButton ${className}`}
      {...props}
    >
      {isLoading && <BsArrowRepeat className="spinning" />}
      {props.children}
    </RedBlockButton>
  );
}

export default LoaderButton